import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDir = await getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  var box = await Hive.openBox('testBox');
  runApp(MyApp());
}







class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}





class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var box;

  @override
  void initState() {
    // TODO: implement initState

    box = Hive.box('testBox');

    super.initState();
  }

  @override
  void dispose(){
    Hive.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hive Test 2"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                box.put('name', 'dileep');
              },
              child: Text("Add data to box"),
            ),
            RaisedButton(
              onPressed: () async{
                await box.delete('name');
                print("DELETED");
              },
              child: Text("print Specific data"),
            ),


            StreamBuilder(
              stream: Hive.box('testBox').watch(),
              builder: (context, snapshot){
                if(snapshot.connectionState == ConnectionState.active && snapshot.hasData){
                  return Text(snapshot.data.value.toString());
                }else{
                  return Text("Loading ...");
                }
              },
            )


//             StreamBuilder(
//   valueListenable: Hive.box('settings').watch(),
//   builder: (context, box, widget) {
//     // build widget
//   },
// )
          ],
        ),
      ),
    );
  }
}
